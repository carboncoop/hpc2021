---
title: "Phase 1: Test Phase (December 2021)"
description: "What does it mean to take part in the Heat Pump Challenge?"
date: 2021-11-23T08:39:37Z
weight: 30
---

If you have filled in our registration form then you have already taken the first step in the Heat Pump Challenge. By applying and letting us know some basic information about your home heating system we are putting together the first pieces in the puzzle.

There are just a few easy steps to get you started with the challenge: 

### 1. Sign up to PowerShaper Monitor

If you have a smart meter and have not yet joined [PowerShaper Monitor](https://powershaper.io) now is the time to do so! This service is free for Carbon Co-op members (or £12 a year otherwise) and is a great way to track your home energy use and domestic carbon emissions. You can [read more about the service at PowerShaper.io](https://powershaper.io). By connecting your smart meter to our system not only will you be able to view your home energy use, but we will be able to better evaluate the impact of this challenge on home energy use. Don’t worry if you don’t have a smart meter, or are unable to connect to PowerShaper Monitor, you can still take part in the challenge, we just won’t be able to get that extra bit of accuracy.


### 2. Adjust your boiler flow temperature

To start with you need to try to adjust the radiator flow temperature on your boiler. During the test phase we would like you to try reducing this to 60C. At this temperature you are unlikely to have any issues bringing your home up to a comfortable temperature but it may take slightly longer than normal (for those with newer condensing boilers this actually is often the temperature at which they are designed to operate!).

Adjusting your boiler can be intimidating, but these changes are safe and easy. [We have created some rough guides about how it might work](/docs/flowtemp), but if you are still struggling, just send us an email and we can help you through the process. 

**PLEASE NOTE**: If your have a hot water tank/cylinder and your boiler does not have the ability to separately adjust your radiator flow temperature and the temperature of hot water sent to the tank then unfortunately you will not be able to participate in the challenge. This is because the water in your tank needs to be heated to at least 60C to prevent the formation of legionella bacteria which can be harmful to your health. If you do not have a hot water tank then this is not an issue as the water is heated instantly and not stored before going to your taps (so there is no opportunity for legionella bacteria to form). 

### 3. Adjust your radiators

If you have TRV (Thermostatic Radiator Valves) installed on your radiators you can skip this step! If you are not sure if TRV’s are installed, [here is how you can find out](/docs/trvs). 

Otherwise, if you do not have TRVs installed on some of your radiators, then we advise that you fully open all radiator valves (turn them up to the max). You will then likely need to adjust these valves to ensure a comfortable temperature.
