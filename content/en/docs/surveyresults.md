---
title: "Findings so far"
description: "Our initial findings from the first two Heat Pump Challenge surveys."
date: 2022-04-04T14:50
weight: 70
---

This short presentation gives a collated view of the responses to our first two Heat Pump Challenge Surveys. These results have not undergone analysis and are presented for your interest only. As the challenge comes to an end, we will be undertaking a further analysis of the results as well as a normalisation of the energy data which we hope will provide a deeper understanding of our homes, heating systems, and the extent to which we are 'heat pump ready'.

### Survey #1
*Our initial Heat Pump Challenge survey*
<object data="/files/HPC_Survey_1.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="/files/HPC_Survey_1.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="/files/HPC_Survey_1.pdf">Download PDF</a>.</p>
    </embed>
</object>

### Survey #2
*Our second Heat Pump Challenge survey, undertaken in Februrary 2022*
<object data="/files/HPC_February_Survey.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="/files/HPC_February Survey.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="/files/HPC_February_Survey.pdf">Download PDF</a>.</p>
    </embed>
</object>
