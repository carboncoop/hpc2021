---
title: "What is the Heat Pump Challenge?"
description: "What is the purpose of heat pump challenge?"
date: 2021-11-23T10:30:22Z
weight: 20
---
The Heat Pump Challenge 2021 is a citizen science and participative research and discovery project led by [Carbon Co-op](https://carbon.coop) helping our member households currently on gas to engage with and understand how installing heat pumps will have an impact on their home.

Participants in the challenge will, in a limited way, simulate the operation of a heat pump replacing their gas boiler by turning down the temperature of hot water going to their radiators (emitters). This simulates how a heat pump would work with current heating systems as heat pumps generally operate at lower flow temperatures. It can also lead to improvements in the efficiency of some gas heating systems, particularly newer condensing boilers which operate more efficiently at lower temperatures. By doing this we hope to promote better understanding of how heating systems work and the changes that will need to be made to get them ready for heat pumps. 

We will be closely monitoring the experience of people who are participating in the challenge and in some cases installing home environment monitors to gather detailed data on how our homes change over the course of the heating season. 

For people taking part in the challenge as organised by Carbon Co-op we will provide incentives for active participation and engagement. 

If you missed the application period you can still participate in the challenge by following the instructions on this website. 

The challenge is intended to be open and participative. You can even suggest changes to this website by following the relevant links in the right side bar.

Whether you are 'officially' part of the challenge or not please get involved in discussion about it on our [community forum](https://community.carbon.coop) and/or by messaging us on Twitter (https://twitter.com/carboncoop).
