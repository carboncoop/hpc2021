---
title: "How do I know if I have TRVs?"
description: "Some simple steps to identify if you have TRVs in your home"
date: 2021-11-23T09:33:09Z
weight: 45
---

![TRV on Radiator](/images/radiatorTRV.jpg)

Thermostatic Radiator Valves or TRVs are used to control the temperature of a room by automatically adjusting the hot water coming into a radiator. For the Heat Pump Challenge, TRVs are great, because they mean that your radiators will automatically be adjusted accounting for the lower flow temperature.

Finding out if your radiator has TRVs can be tricky, and many people are unaware that the even have them, *so how can you tell*? 

Most thermostatic radiator valves have number marks from 1 - 5 but some have a screen and buttons. If you have noticed a snowflake or ‘defrost’ symbol (❄) this is also an indicator that you have a TRV fitted. Generally TRVs can be removed from the radiator by unscrewing with your hands, and do not require any tools (we do not suggest removing them!). In contrast, manual radiator valves do not have numbered marks, instead generally having a plus (+) and minus (-) and cannot be removed from the radiator without tools.
