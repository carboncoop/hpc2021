---
title: "When is the Challenge taking place?"
description: "More information about the dates and phases of the heat pump challenge"
date: 2021-11-22T13:11:51Z
weight: 20
type: docs
---

The heat pump challenge will take place over the current ‘heating season’, the period in which people typically use their heating. That means you can start now, and we will be finishing as the weather warms in the spring. 

The challenge will be broken down into several phases, and during each phase we may ask you to do different things. The rough phases are outlined here: 

### **Phase 1**: End of November until early January // Setup and testing
During this phase we will be helping to get you set up with the challenge. We will not be recording any data, we may contact you directly to check that you are comfortable with the plans, have been able to adjust your boiler as necessary and not had any unexpected problems. In some cases we may contact you about installing temperature and humidity sensors around your home, so that in later phases we can better assess your home’s performance.

### **Phase 2**: Mid January until Mid February // First evaluation run
During this phase we will ask you to keep a record of your experiences with the adjusted heating, and we may contact some of you directly to ask more specific questions about your home and heating. We may also send out a survey for you to complete. 

### **Phase 3**: Mid February until March // Second Tests
Based on the information collected during the first trial we may suggest some changes to the heating, and will have a further evaluation period where we pay close attention to your experience and make further surveys. This period will run until the end of the heating season - most likely mid March.
