---
title: "How do I adjust my flow temperature?"
description: "General tips and information about adjusting your boiler flow temperature"
date: 2021-11-23T08:54:37Z
weight: 40
---
There are a few different ways you might be able to adjust the flow temperature on your boiler and it will depend on the manufacturer and model. You may be able to find the specific user manual for your boiler, with instructions on changing the flow temperature [on this site](https://www.freeboilermanuals.com/user-manuals/). We would always recommend you refer to your operating manual, but here are some of the general methods that are often used.

A key thing to look for with your boiler is the radiator symbol and the tap symbol. Usually the radiator symbol denotes flow temperature whereas the tap symbol marks the domestic hot water (DHW) settings. For our purposes we do not want to adjust the DHW and instead only change the flow temperature for the heating. See these examples below: 

![tap and radiator symbols](/images/tap-radiator.png)

### Differences between system and combi boilers
There are two main types of gas heating system in use in UK - system and combi - which relates to how hot water is supplied to outlets in home. Boilers for both types look very similar or are visually identical. 

System type supply hot water to a cylinder/tank which then supplies hot water to outlets. These are more common in older and / or larger properties with larger numbers of hot water outlets (taps/showers)/more people.

Combi type supply hot water directly from the gas boiler. These are more common in smaller and / or newer properties with smaller numbers of outlets/less people.

**IMPORTANT** If you have a system type it is important you dont reduce your DHW below 60°C. This is important to prevent the growth of legionella bacteria in your tank.

If you have a system type boiler it may not be possible to control your DHW temperature separately to your radiator flow temperature, in this case regretabbly you may not be able to fully participate in the heat pump challenge (however you could get down to 60C which may still be a reduction).

### **Boiler Controls Type 1**: Manual dials, separate hot water and heating with digital temperature display

Some boilers have two dials on the front to separately control the hot water and space/flow heating temperatures. You may instead have a single dial with the radiator symbol which will work the same way.

![Dual Boiler Controls Image](/images/dualcontrolsDWC-CH.png)

The dial with a radiator like symbol (left in the above image) will control the temperature of the heating whilst the right will control the hot water temperature. 

As you turn the relevant dial the temperature on the digital display may change indicating the new setting. 

### **Boiler Controls Type 2**: Manual dials only, with number scale

Some boilers may have a numbered dial like below, for these boilers if changing the dial does not change the temperature displayed on the digital display, or there is no digital display it may be worth checking your [boiler user manual](https://www.freeboilermanuals.com/user-manuals/) since some models (in particular Worcester Bosch Greenstar boilers) include a table outlining corresponding temperatures to the numbers on the dial.

![Numbered Dial Image](/images/numbereddial.png)

With boilers where you have manual dials, and a digital display, but the change in temperature is not displayed as the dial is turned, you may still be able to figure out what temperature it is set to by observing the display when the heating is on and making adjustments as necessary.

If there is no temperature table in the user manual it may not be possible to identify the flow temperature of the boiler without additional external temperature sensors. In such a case please contact us at [info@carbon.coop](mailto:info@carbon.coop) to see if it is possible for us to recommend or supply such sensors.

### **Boiler Controls Type 3**: Manual dial only, without number scale

Some boilers will have manual controls for separate control of the flow temperature and domestic water heating temperatyre, but will have no number scale on the controls and no corresponding digital temperature display.

![Manual dial no numbers](/images/ManualNoNumber.png)

In general, and depending on the model of the boiler the temperature will be adjustable between ~25°C-85°C using these controls. For the purposes of the challenge we want to be around 50-55°C. 

It can be very difficult to accurately determine the flow temperature in this case without the use of additonal temperature sensors. We may be able to supply or reccomend such sensors, in which case please contact us at: [info@carbon.coop](mailto:info@carbon.coop).

### **Boiler Controls Type 4**: Digital interface

Digital interfaces on boilers can vary significantly however, there are a few key things which you can look for.

![Digital Display image](/images/digitaldisplay.png)

First you will need to select the flow temperature function, this is usually indicated by the radiator symbol which will look a bit like one of the icons below:

![Central Heating (radiator) icons](/images/CHicons.png)

After that it is a case of adjusting the target flow temperature to the desired level, often this is done using the down arrow button, or a dial.

### **Boiler Controls Type 5**: Single manual dial, no clear seperate controls for flow temperature

Some boilers may have a manual dial, which may be numbered or even have temperatures marked on it, but does not provide a clear separation of the radiator flow temperature (usually marked with a radiator symbol) and domestic how water temperature (DHW - usually marked with a tap symbol). 

In this case it is **important to ensure that you do not reduce you DHW temperature below 60°C** which is the minimum temperature required in order to kill legionella bacteria.

It may be possible to test if this is a separate flow temperature control or a combined DHW and heating control by temporarily turning the dial to the minimum and running the hot water tap, ensuring the temperature has not been reduced from the usual >60°C.

### **Boiler Controls Type 6**: Flow temp control from external device (likely via OpenTherm control) as part of load/weather compensation

It is possible to control flow temperatures from an external device connected to the boiler where this supports the OpenTherm protocol used for intercommunication with heating systems. An example of such a device is the Nest Thermostat 3rd Gen or E paired with a boiler that supports this (a feature Nest call 'True Radiant'). In this case your OpenTherm enabled thermostat is already regulating flow temperatures to improve the performance of the boiler and your comfort and manually adjusting the flow temperatures from the boiler may not have any effect. For some boilers it is still possible to influence the behaviour in this case by setting a max flow temperature on the boiler itself. Please contact us if you would like to try and participate in the challenge and we can offer more tailored advice in this case.

### **Boiler Controls Type 7**: Weather compensation integrated into boiler

Some boilers (e.g. Viessmann Vitocal 200) offer weather compensation integrated into the boiler and facilitated by the installation of an external temperature sensor. This will typically involve setting a heating curve which calibrates the flow temperature to the outside temperature. If you have never changed these settings you may find that the maximum flow temperature has been set to quite a high value in which case there would be a similar benefit to reducing this. This is effectively like manually changing your flow temperature as the weather changes except in this case you are programming this into the boiler so that you do not need to remember to do it. Please contact us if you would like to try and participate in the challenge and we can offer more tailored advice in this case.  
