---
title: "Introduction"
linkTitle: "Discover the Heat Pump Challenge 2021"
weight: 20
menu:
  main:
    weight: 20
---

Here is where you can find all the information that you might need about the Heat Pump Challenge 2021, how you can take part, and what it involves. 

If you have a specific question, or would like more information about the heat pump challenge you can contact us directly at [info@carbon.coop](mailto:info@carbon.coop)
