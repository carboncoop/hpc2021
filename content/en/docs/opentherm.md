---
title: "Boiler controls"
description: "Further information about the different kinds of boiler controls."
date: 2021-12-04T08:39:37Z
weight: 60
draft: true
---
# Introduction

Control of heating systems is a surprisingly complicated area with a wide range of solutions addressing different things such as comfort and operational efficiency. To help understand it is important to distinguish between the types of signals used to control boilers and the control algorithms used.

# Control signals 
Generally the signals used to control the boiler can be divided into 3 possible types:

## On/Off

## OpenTherm

## Manufacturer specific (e.g. Vailant eBUS)

# Control algorithms
Unlike with boiler signalling, the control algorithms used can be mixed and matched to a certain extent by different products. For example, the Nest 3/E thermostats can use smart, load, and weather compensation together. Or more tradiationally, a room thermostat with a 7 day programmer which allows for different setpoints to be used at different times (e.g. night set back or lower during day). 

It is also worth bearing in mind that the boiler has its own internal control which modulates its outputs to achieve a given flow temperature and manage the difference between flow/return.

## No algorithm (manual/timer operation)
The boiler can just be turned on/off manually, either with a switch or a mechanical timer.

## Simple setpoint (single or multi zone)
A thermostatic control sends the boiler an on/off signal based on whether or not heating area(s) have reached a certain temperature. Usually there is some hysteresis built in to prevent short cycling of the system.  

## Time Proportional & Integral
This is a more sophisticated form of setpoint based control where the room thermostat unit learns, based on a log of previous temperature sensor data, how much longer the boiler will need to heat up a room once the room temperature enters the 'proportional' region (typically around 1.5C the setpoint). This can help to prevent the temperatures from overshooting and reduce the energy use of the boiler. See [this](https://youlearn.honeywellhome.com/uploads/documents/FAQ_-_TPI_functionality.pdf) Honeywell document for a more detailed write up.

## Load compensation

## Weather compensation

## 'Smart'

OpenTherm is a protocol for communication between heating controls and heating units/systems. Unlike previous protocols which were 'analogue' in nature and based on the presence or level of a voltage signal to control heating systems, the OpenTherm protocol is 'digital' sending richer data and more complex control signals. This can facilitate a wider and more complex range of types of control of which load and weather compensation is one variety.

# What is ErF?

# What is Boiler Plus?



