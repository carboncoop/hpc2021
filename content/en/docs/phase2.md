---
title: "Phase 2: Evaluation run (January 2022)"
description: "Reducing flow temperature to within heat pump range"
date: 2022-01-27T08:39:37Z
weight: 32
---

We've had a brilliant response so far, with over over 95% of survey respondents able to reduce the flow temperature on their boiler to 60°C during the testing phase. Most participants reduced their flow temperature by between 5-25°C we should see some significant efficiency gains and are interested to see what impact this will have on gas usage. Thank you so much to everyone who has fed back so far, it’s all really useful learning. So now is the time for Phase 2, which will be a little more challenging.


### 1. Tell us about your experience in the testing phase

Most of you have already responded to the survey we sent out at the end of last year, many thanks if that includes you. If you've not yet had time to fill out your experiences, you can still [complete the testing phase survey here](https://lay8txb2htj.typeform.com/hpc-stage1).


### 2. Adjust your flow temperature

In phase 2 we would like you to try reducing your flow temperature all the way down to 50°C. At this temperature you may have problems reaching your desired temperature, particularly if you live in an older property, but we have chosen this level as it is within the operating range of a standard heat pump. It will take longer for your home to reach temperature when the heating first comes on and you may find it necessary to adjust your heating periods to account for this. 

If you find 50°C isn’t sufficient to get your home up to desire temperature, you should increase your flow temperature to 55°C. Feel free to fallback to 60°C or above if youn need to, the aim is to find the most efficient (lowest) temperature you can run your boiler at while still keeping your home cosy.


**PLEASE NOTE**: If your have a **hot water tank/cylinder** and your boiler does not have the ability to separately adjust your radiator flow temperature and the temperature of hot water sent to the tank then unfortunately you will not be able to participate in the challenge. If you have seperate controls but have any concern that changes made to your flow temperature have made an impact on your hot water temperature, you should **NOT make reductions below 60°C**. It is vital to ensure the water in your tank is heated to at least 60°C, as per [HSE guidelines](https://www.hse.gov.uk/healthservices/legionella.htm), to prevent the formation of legionella bacteria which can be harmful to your health.

### 3. Keep a record of your changes 

We're interested in the impact flow temperatures have on comfort levels around the home and also on gas consumption. It is useful for us to have an idea when flow temperatures were changed, any other changes you made to your heating system setup and any impact you noted e.g was it warmer or colder in particular parts of your home, was there a particular day the boiler seemed to struggle to deliver your usual room temperature?

For people who've been successful in connecting their smart meters to [PowerShaper Monitor](https://powershaper.io), we aim to plot energy usage against local weather data to assess what impact flow temperature reductions has had on your gas usage. 

