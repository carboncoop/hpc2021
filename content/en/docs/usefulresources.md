---
title: "Useful resources"
description: "Some resources that we have found useful or that others have suggested."
date: 2021-11-24T18:30:00Z
weight: 50
---

Some useful resources we have collected or which have been suggested. Will update as we go along.

- [BETATALK - THE RENEWABLE ENERGY AND LOW CARBON HEATING PODCAST](https://betatalk.buzzsprout.com/) A series of podcasts about heat pumps and renewable heat in the UK.
- [The Heating Hub - Save up to 8% on your gas bills, by turning down the 'flow' temperature](https://www.theheatinghub.co.uk/articles/turn-down-the-boiler-flow-temperature) An insightful article on the potential benefits of turning down your gas boiler temperature.
- [John Cantor - Heat pumps without the hype](https://heatpumps.co.uk/) A wealth of information from John's decades of experience working with heating systems.
