Microsite for Heat Pump Challenge 2021
======================================

This is the Hugo source backing the Heat Pump Challenge 2021 microsite deployed via netlify (https://hpc2021.netlify.app/) to https://hpc.carbon.coop .

The site uses the docsy theme (https://www.docsy.dev/). 

Content can be added (by authorised users) by navigating to the site and then clicking the links to add / edit pages via Gitlab. Tips on structuring, styling, and syntax appropriate can be found (in the first instance) on the docsy docs site: https://www.docsy.dev/docs . Ultimately it is a Hugo site and most normal conventions for Hugo sites apply. 

The deployment largely follows the instructions found on the docsy website: https://www.docsy.dev/docs/deployment/#deployment-with-netlify . Configuration of the build via the netlify.toml is required (netlify UI configuration with the given options does not work at time of writing).

